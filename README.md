# gitlab-runner

docker-compose files for running a gitlab-runner used for running Alpine CI/CD
jobs

## Setup

Copy the `example.env` file to`.env` and fill in the values.

* `GITLAB_REGISTRATION_TOKEN_SHARED` - Gitlab CI/CD Shared runner token. This
  runner can be used for any kind of CI/CD job.
* `GITLAB_REGISTRATION_TOKEN_DOCKER` - Gitlab CI/CD token that is just used for
  jobs that need to build docker images. Jobs in this runner have access to the
  docker socket, so have potentially root access to the host it's running on.
* `GITLAB_REGISTRATION_TOKEN_PROJECT` - The registration token of one project
  that builds docker images. This runner can later be assigned to other projects.
* `ALPINE_ARCH` - The architecture this runner is running on. This will appear in the
  description for this runner and is added as a tag to limit jobs to specific
  arches.

